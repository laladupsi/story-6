from selenium import webdriver
from django.test import Client
import time
import unittest

class VisitWebTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    #test for title
    def test_has_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Issa Main Menu', self.browser.title)
        time.sleep(3)

    #test link is in content div
    def test_content_div(self):
        self.browser.get('http://localhost:8000')
        content_div = self.browser.find_element_by_class_name('content')
        time.sleep(3)
        self.browser.quit() #if the browser quits, it has found the content div

    #test for main menu css
    def test_main_menu_page_using_css(self):
        #saya emg ga make css...
        pass

    #test for challenge css
    def test_challenge_page_using_css(self):
        #saya emg ga make css...
        pass
    
    #test to submit form
    def test_can_submit_form(self):
        
        self.browser.get('http://localhost:8000')
        form_page = self.browser.find_element_by_id('form_link')
        form_page.click()
        time.sleep(3)
        
    
        description = self.browser.find_element_by_name('desc')
        submit = self.browser.find_element_by_id('submit')
        # Fill the form with data
        description.send_keys('Lab kali ini membahas tentang CSS dengan penggunaan Selenium untuk Test nya')
        # submitting the form
        time.sleep(3)
if __name__ == '__main__':
    unittest.main(warnings='ignore')
