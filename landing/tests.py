from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import StatusForm

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code,200)
    
    def test_landing_using_index_func(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, index)

    def test_landing_can_create_new_status(self):
        new_status = Status.objects.create(desc='ngantuk bat bung')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status,1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="status-form-input"',form.as_p())
        self.assertIn('id="id_desc"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'desc':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            'Tolong isi input ini'
        )

    def test_landing_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/landing/posted',{'desc':test})
        self.assertEqual(response_post.status_code,302)

        response = Client().get('/landing/')
        html_response  = response.content.decode('utf8')
        self.assertIn(test,html_response)
'''
    def test_landing_post_error_and_render_the_result(self):
        test = 'a'*99
        response_post = Client().post('/landing/posted',{'desc':test})
        self.assertEqual(response_post.status_code,302)

        response = Client().get('/landing/')
        html_response  = response.content.decode('utf8')
        self.assertNotIn(test,html_response)
'''
