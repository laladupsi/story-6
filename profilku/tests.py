from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class ProfilkuUnitTest(TestCase):
    def test_profilku_url_is_exist(self):
        response = Client().get('/profilku/')
        self.assertEqual(response.status_code,200)

    def test_profilku_using_index_func(self):
        found = resolve('/profilku/')
        self.assertEqual(found.func,index)

    def test_profilku_using_profil_template(self):
        response = Client().get('/profilku/')
        self.assertTemplateUsed(response,'profil.html')