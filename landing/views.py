from django.shortcuts import render
from .forms import StatusForm
from .models import Status
from django.http import HttpResponseRedirect

# Create your views here.
'''
status_page = {}
def index(request):
    return render(request,'landing_page.html', {'status_page':status_page})

def message_post(request):
    form = StatusForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['desc'] = request.POST['desc']
        message = StatusForm(desc=response['desc'])
        message.save()

        html = 'landing_page.html'
        return render(request,html,response)
    else:
        return render(request,'landing_page.html',response)
'''
response = {}
def index(request):
    form = StatusForm()
    response['form'] = form
    hasil = Status.objects.all()
    response['result']=hasil
    return render(request, 'landing_page.html', response)

def post(request):
    form = StatusForm(request.POST or None)
    if (request.method == "POST"):
        print(form)
        response['desc'] = request.POST['desc']
        message = Status(desc=response['desc'])
        message.save()
        
    else:
        print(form.errors)
    
    return HttpResponseRedirect('/landing')

