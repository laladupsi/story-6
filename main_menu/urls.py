from django.contrib import admin
from django.urls import include,path
from django.views.generic import RedirectView
from .views import index

urlpatterns = [
    path('',index)
]