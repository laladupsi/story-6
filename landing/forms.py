from django import forms

class StatusForm(forms.Form):
    errors = {
        'description': 'Tolong isi input ini',
    }
    attrs = {
        'class':'status-form-input',
        'placeholder':'What is on your mind?',
    }
    desc = forms.CharField(label="",max_length=60, widget=forms.Textarea(attrs=attrs),
        required=True)